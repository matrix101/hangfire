﻿using IdentityServer.Models;
using IdentityServer4;
using IdentityServer4.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace IdentityServer.Services
{
    public class ClientsService
    {
        private readonly IdentityServerContext context;
        private readonly ILogger<ClientsService> logger;

        public ClientsService(ILogger<ClientsService> logger, IdentityServerContext context)
        {
            this.context = context;
            this.logger = logger;
        }

        public int CreateClient(Client client)
        {
            logger.LogInformation("Creating a new client");
            var existingClient = context.Clients.FirstOrDefault(x => x.ClientId == client.ClientId);
            if (existingClient is null)
            {
                try
                {
                    if (context.ApiResources.FirstOrDefault(x => x.Name == "api") is null)
                        context.ApiResources.Add(new ApiResources
                        {
                            Name = "apiresource",
                            Created = DateTime.Now,
                            Enabled = true,
                            ApiScopes = new List<ApiScopes> {
                            new ApiScopes {
                                DisplayName = "Api",
                                Name = "api",
                                ShowInDiscoveryDocument = true,
                                ApiScopeClaims = new List<ApiScopeClaims>
                                {
                                    new ApiScopeClaims { Type = "api" }
                                }
                            }
                        }
                        });
                    context.Clients.Add(new Clients
                    {
                        ClientId = client.ClientId,
                        ClientName = client.ClientName,
                        ProtocolType = client.ProtocolType,
                        AbsoluteRefreshTokenLifetime = 2592000,
                        AccessTokenLifetime = 2592000,
                        AccessTokenType = (int)client.AccessTokenType,
                        AllowAccessTokensViaBrowser = client.AllowAccessTokensViaBrowser,
                        AllowOfflineAccess = client.AllowOfflineAccess,
                        AllowPlainTextPkce = client.AllowPlainTextPkce,
                        AllowRememberConsent = client.AllowRememberConsent,
                        AlwaysIncludeUserClaimsInIdToken = true,
                        AlwaysSendClientClaims = true,
                        AuthorizationCodeLifetime = 2592000,
                        IdentityTokenLifetime = 2592000,
                        BackChannelLogoutSessionRequired = client.BackChannelLogoutSessionRequired,
                        BackChannelLogoutUri = client.BackChannelLogoutUri,
                        ClientClaims = new List<ClientClaims> { },
                        ClientClaimsPrefix = client.ClientClaimsPrefix,
                        ClientCorsOrigins = new List<ClientCorsOrigins> { },
                        ClientIdPrestrictions = new List<ClientIdPrestrictions> { },
                        ClientPostLogoutRedirectUris = new List<ClientPostLogoutRedirectUris> { },
                        ClientProperties = new List<ClientProperties> { },
                        NonEditable = false,
                        ClientSecrets = new List<ClientSecrets> { new ClientSecrets {
                            Value = client.ClientSecrets.First().Value.Sha256(),
                            Type = IdentityServerConstants.SecretTypes.SharedSecret,
                        } },
                        ClientGrantTypes = new List<ClientGrantTypes> { new ClientGrantTypes { GrantType = string.Join(",", GrantTypes.ClientCredentials) },

                    },
                        Enabled = true,
                        RequirePkce = false,
                        RequireClientSecret = true,
                        ClientScopes = { new ClientScopes { Scope = "api" } }
                    });
                    return context.SaveChanges();
                }
                catch (Exception e)
                {
                    logger.LogError(e, "Failed to create a new Client");
                }
            }
            throw new ExistingException($"Client {client.ClientId} already exists");
        }
    }
}
