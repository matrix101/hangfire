﻿using System;
using System.Runtime.Serialization;

namespace IdentityServer.Services
{
    [Serializable]
    internal class ExistingException : Exception
    {
        public ExistingException()
        {
        }

        public ExistingException(string message) : base(message)
        {
        }

        public ExistingException(string message, Exception innerException) : base(message, innerException)
        {
        }

        protected ExistingException(SerializationInfo info, StreamingContext context) : base(info, context)
        {
        }
    }
}