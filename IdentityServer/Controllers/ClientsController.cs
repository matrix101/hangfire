﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using IdentityServer.Services;
using IdentityServer4.Models;
using Microsoft.AspNetCore.Mvc;

namespace IdentityServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ClientsController : ControllerBase
    {

        private readonly ClientsService clientsService;
        public ClientsController(ClientsService clientsService)
        {
            this.clientsService = clientsService;
        }

        // GET api/values
        [HttpGet]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // POST api/values
        [HttpPost]
        public IActionResult Post([FromBody] Client client)
        {
            try
            {
                if (string.IsNullOrEmpty(client.ClientId))
                    return BadRequest(new { Error = "Client Id cannot be empty" });
                if (string.IsNullOrEmpty(client.ClientSecrets?.FirstOrDefault()?.Value))
                    return BadRequest(new { Error = "Client secret is required" });
                clientsService.CreateClient(client);

            }
            catch (ExistingException e)
            {
                return BadRequest(new { Error = e.Message });
            }
            return Ok();
        }
    }
}
