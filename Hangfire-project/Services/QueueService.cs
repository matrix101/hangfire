﻿using HangfireProject.Models;
using Microsoft.Extensions.Logging;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace HangfireProject
{
    public interface IQueueService
    {
        void Execute(Guid guid);
        string GetQueueStatus(int id);
    }
    public class QueueService : IQueueService
    {
        private readonly HangFireContext context;
        private readonly ILogger<QueueService> logger;

        public QueueService(HangFireContext context, ILogger<QueueService> logger)
        {
            this.context = context;
            this.logger = logger;
        }

        public HangFireContext Context => context;

        public void Execute(Guid guid)
        {
            try
            {
                logger.LogInformation($"Job started {guid}");
                Thread.Sleep(new Random().Next(5, 15) * 1000);
                logger.LogInformation($"Job finished {guid}");
            }
            catch (Exception e)
            {
                logger.LogError(e, "Error");
            }
        }

        public string GetQueueStatus(int id)
        {
            var job = context.Job.FirstOrDefault(x => x.Id == id);
            return job.StateName;
        }
    }
}
