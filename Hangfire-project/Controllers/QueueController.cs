﻿using System;
using Hangfire;
using HangfireProject.Models;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;

namespace HangfireProject
{
    [Route("api/[controller]")]
    [ApiController]
    public class QueueController : ControllerBase
    {
        private readonly IQueueService queueService;

        public QueueController(IQueueService queueService)
        {
            this.queueService = queueService;
        }
        [HttpPost]
        public IActionResult CreateJob()
        {
            var guid = Guid.NewGuid();
            var id = BackgroundJob.Enqueue(() => queueService.Execute(guid));

            return Ok(new { Result = $"Job Created {id}" });
        }

        [HttpGet]
        public IActionResult GetJobStatus(int id)
        {
            return Ok(new { Result = queueService.GetQueueStatus(id) });
        }
    }
}